alter table links_categories add        collapsed int(1) unsigned default 1;
alter table links_categories add        shown int(10) unsigned default 10;
alter table links_categories add        parent int(10) unsigned;
alter table links_links add active int(1) unsigned default 1;
alter table links_links add clicks int(10) unsigned default 0;
alter table links_links add         dead int(1) unsigned default 0;
alter table links_links add         deadcomment varchar(255);
alter table links_links add         deadby int(10) unsigned;

